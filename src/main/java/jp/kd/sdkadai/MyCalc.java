package jp.kd.sdkadai;
import java.math.*;

public class MyCalc {
	private int num;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int ex1(int kake){
		if(kake < 0){
			kake *= -1;
		}
		return num * kake;
	}

	public boolean ex2(){
		boolean flag = true;

		if(num <= 1){
			flag = false;
		} else {
			for(int i = 2 ; i < num ; i++){
				int yaku = num % i;
				if(yaku == 0){
					flag = false;
				}
			}
		}
		return flag;
	}

	public String ex3(int x){
		String numStr = String.valueOf(x);
		BigInteger bigint = new BigInteger(numStr);
		BigInteger kake = new BigInteger(numStr);
		long number = x;
		
		for(int i = 1 ; i < x ; i++){
			bigint = bigint.multiply(kake);
			System.out.println("bigint=" + bigint);
			System.out.println("kake=" + kake);
		}
		numStr = String.valueOf(bigint);
		return numStr;
	}
}
